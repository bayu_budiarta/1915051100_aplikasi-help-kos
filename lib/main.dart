import 'package:flutter/material.dart';
import 'rekomendasikos.dart';
import 'call.dart';

void main(List<String> args) {
  runApp(new MaterialApp(
    home: MyApp(),
    theme: ThemeData(fontFamily: 'Raleway'),
    routes: <String, WidgetBuilder>{
      '/menu': (BuildContext context) => new MyApp(),
      '/rekomendasikos': (BuildContext context) => new Rekomendasikos(),
      '/call': (BuildContext context) => new Call(),
    },
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("HELP KOS", style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
        centerTitle: true,
        backgroundColor: Colors.green[900],
      ),
      backgroundColor: Colors.green[100],
      drawer: new Drawer(
          child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: new Text(
              "HELP KOS",
              style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                  fontFamily: 'Raleway'),
            ),
            accountEmail: new Text("helpkossgr@gmail.com"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage('gambar/help kos.jpg'),
            ),
          ),
          ListTile(
            leading: Icon(Icons.notifications),
            title: Text("Notifikasi"),
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text("Keluar"),
          ),
        ],
      )),
      body: Container(
          padding: EdgeInsets.all(30),
          child: GridView.count(
            crossAxisCount: 2,
            children: <Widget>[

              Card(
                margin: EdgeInsets.all(8),
                child: InkWell(
                  onTap: () => Navigator.of(context).pushNamed('/rekomendasikos'),
                  splashColor: Colors.pink,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.house_outlined,
                          size: 100,
                          color: Colors.black,
                        ),
                        Text("ReKos",
                            style: new TextStyle(
                                fontSize: 17, fontFamily: 'Raleway'))
                      ],
                    ),
                  ),
                ),
              ),

              Card(
                margin: EdgeInsets.all(8),
                child: InkWell(
                  onTap: () => Navigator.of(context).pushNamed('/call'),
                  splashColor: Colors.pink,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.chat_rounded,
                          size: 100,
                          color: Colors.red,
                        ),
                        Text("Tanya Kos",
                            style: new TextStyle(
                                fontSize: 17, fontFamily: 'Raleway'))
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )),
      bottomNavigationBar: BottomAppBar(
        //color: Colors.transparent,
        child: Container(
          height: 30,
          color: Colors.green[900],
          alignment: Alignment.center,
          child: Text(
            'Developed by Bayu Budiarta',
            style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontFamily: 'Raleway'),
          ),
        ),
        //elevation: 0,
      ),
    );
  }
}
