import 'package:flutter/material.dart';

class Rekomendasikos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title:Text("Kos Singaraja"),
        centerTitle: true,
        backgroundColor: Colors.green[900],
      ),
      body: new Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/1.jfif'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Teleng 4",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 3,5",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Teleng No.20, Banyuasri, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Lemari, Kamar Mandi Dalam, CCTV, Sprai, Dapur Dalam",style: TextStyle(fontSize: 11,),),

                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/2.jpg'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Banyuning 4",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 4",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Srimadya, Banyuning Barat, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Lemari, Kasur, Washtafel, Dalam Dalam, Kamar Mandi Dalam",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/3.jpg'),fit: BoxFit.cover,)
                  ),

                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Sahadewa 1",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 3,5",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Sahadewa No.1, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja, Kursi, Lemari, Kamar Mandi Dalam, Dapur Dalam, Parkiran Luas",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/4.jpg'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Wijaya Kusuma 4",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 3,5",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Wijaya Kusuma 4. Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja, Kursi, Lemari, Kamar Mandi Dalam, Jemuran",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/5.jpg'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Sahadewa 2",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 3 x 3",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Sahadewa 3, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Kamar Mandi Dalam, Dapur di Luar, Jemuran",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/6.jpg'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Putri Kutilang 1",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 4",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Kutilang 1, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja Rias, Kursi, Lemari, Kamar Mandi Dalam, Dapur Dalam, Parkiran Luas",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/7.jpg'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Wijaya Kusuma 5",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 3",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Wijaya Kusuma 5, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Dipan Kasur, Meja, Kursi, Lemari, Kamar Mandi Dalam, CCTV",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/8.jpg'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Rama 1",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 4",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Srikandi 1, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja, Kursi, Lemari, Kamar Mandi Dalam, TV-Parabola, AC",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/9.jpg'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Wijaya Kusuma 6",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 3",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Wijaya Kusuma 6, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja, Kursi, Lemari, Kamar Mandi, CCTV, Free Wifi",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/10.jfif'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Elite Rajawali",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 3",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Rajawali 7, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja, Kursi, Lemari, Kamar Mandi Dalam, Tv-Parabola, Free Wifi, AC",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/11.jfif'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Sambangan Asem B1",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 4",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Srikandi 11, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja, Kursi, Lemari, Kamar Mandi, Free Wifi, Washtafel",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/12.jfif'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Abimanyu 1",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 4",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Abimayu 2 No.1, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur Dipan, Meja, Kursi, Lemari, Kamar Mandi Dalam, CCTV, Free Wifi",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/13.jpg'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos GN Rinjani 1",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 4",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Gn Rinjani, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja, Kursi, Lemari, Kamar Mandi Dalam, Bantal, Free Wifi, Washtafel",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    )
                  )
                ],
              ),
            ),
               Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/14.jfif'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Banyuning 5",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 4 x 3",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Banyuning Barat 5, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja, Kursi, Lemari, Kamar Mandi Dalam, Laci",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    ),
                    
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.greenAccent,
                    child: new Image(image: NetworkImage('gambar/15.jfif'),fit: BoxFit.cover,)
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: 200,
                      color: Colors.green[200],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Kos Wijaya Kusuma 3",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kamar Ukuran 3 x 3",style: TextStyle(fontSize: 11,),),
                          Text("Jl. Wijaya Kusuma 3, Kec. Buleleng, Kabupaten Buleleng, Bali 81116",style: TextStyle(fontSize: 11,),),
                          Text("_____________________________________________________",style: TextStyle(fontSize: 7,),),
                          Text("Fasilitas:",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                          Text("Kasur, Meja, Kursi, Lemari, Kamar Mandi Dalam",style: TextStyle(fontSize: 11,),),
                        ],
                      ),

                    ),
                    
                  )
                ],
              ),
            ),
            

          ],
        ),
      ),
    );
  }
}