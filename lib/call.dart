import 'package:flutter/material.dart';
import 'callthanks.dart';

class Call extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: Text("Pertanyaan dan Bantuan"),
          centerTitle: true,
          backgroundColor: Colors.green[900],
        ),
        body: new ListView(
          children: <Widget>[
            Container(
                padding: EdgeInsets.all(15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: Image(image: NetworkImage('gambar/16.png'),
                        width: 120,
                        height: 120,
                      ),
                    ),
                    Text(
                      "Sampaikan Pertanyaan Anda Agar Help Kos Bisa Membantu Anda",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.teal,
                          fontFamily: 'Raleway'),
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    new TextField(
                      decoration: new InputDecoration(
                          labelText: "Nama Lengkap",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20),
                          )),
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    new TextField(
                      decoration: new InputDecoration(
                          labelText: "No HP",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20),
                          )),
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    new TextField(
                      decoration: new InputDecoration(
                          labelText: "Email",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20),
                          )),
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    new TextField(
                      maxLength: 400,
                      maxLines: 2,
                      decoration: new InputDecoration(
                          labelText: "Pertanyaan",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20),
                          )),
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    new RaisedButton(
                      child: new Text("Kirim"),
                      color: Colors.green,
                      onPressed: () {
                        var route = new MaterialPageRoute(
                          builder: (buildcontext) => new CallThanks(),
                        );
                        Navigator.of(context).push(route);
                      },
                    )
                  ],
                )),
          ],
        ));
  }
}
